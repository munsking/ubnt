--- a/include/image.h
+++ b/include/image.h
@@ -995,17 +995,15 @@ int fit_set_timestamp(void *fit, int nof
  *
  * Also add signatures if signature nodes are present.
  *
  * returns
  *     0, on success
  *     libfdt error code, on failure
  */
-int fit_add_verification_data(const char *keydir, void *keydest, void *fit,
-			      const char *comment, int require_keys,
-			      const char *engine_id);
+int fit_add_verification_data(struct image_tool_params *params, void *keydest, void *fit);
 
 int fit_image_verify(const void *fit, int noffset);
 int fit_config_verify(const void *fit, int conf_noffset);
 int fit_all_image_verify(const void *fit);
 int fit_image_check_os(const void *fit, int noffset, uint8_t os);
 int fit_image_check_arch(const void *fit, int noffset, uint8_t arch);
 int fit_image_check_type(const void *fit, int noffset, uint8_t type);
@@ -1065,18 +1063,26 @@ void image_set_host_blob(void *host_blob
 
 #ifdef CONFIG_FIT_BEST_MATCH
 #define IMAGE_ENABLE_BEST_MATCH	1
 #else
 #define IMAGE_ENABLE_BEST_MATCH	0
 #endif
 
+enum image_sign_type {
+	SIGN_TYPE_RSA_FILE = 0,
+	SIGN_TYPE_AWS,
+};
+
 /* Information passed to the signing routines */
 struct image_sign_info {
 	const char *keydir;		/* Directory conaining keys */
 	const char *keyname;		/* Name of key to use */
+	enum image_sign_type sign_type;	/* Signing type */
+	const char *aws_profile;	/* AWS profile to use */
+	const char *aws_function;	/* AWS function to use */
 	void *fit;			/* Pointer to FIT blob */
 	int node_offset;		/* Offset of signature node */
 	const char *name;		/* Algorithm name */
 	struct checksum_algo *checksum;	/* Checksum algorithm information */
 	struct crypto_algo *crypto;	/* Crypto algorithm information */
 	const void *fdt_blob;		/* FDT containing public keys */
 	int required_keynode;		/* Node offset of key to use: -1=any */
--- /dev/null
+++ b/include/u-boot/aws.h
@@ -0,0 +1,8 @@
+#ifndef _AWS_H
+#define _AWS_H
+
+int aws_sign(struct image_sign_info *info,
+	     const struct image_region region[], int region_count,
+	     uint8_t **sigp, uint *sig_len);
+
+#endif /* _AWS_H */
--- /dev/null
+++ b/lib/aws/aws-sign.c
@@ -0,0 +1,118 @@
+#include <image.h>
+#include <jansson.h>
+#include <sys/wait.h>
+#include <openssl/evp.h>
+#include <unistd.h>
+
+#include "utils.h"
+
+#define DEFAULT_AWS_PROFILE	"default"
+#define DEFAULT_AWS_LAMBDA_FUNC "signer"
+
+int aws_sign(struct image_sign_info *info,
+	     const struct image_region region[], int region_count,
+	     uint8_t **sigp, uint *sig_len)
+{
+	unsigned char *sha1;
+	unsigned int sha1_len;
+	const char *argv[20];
+	json_t *payload, *response = NULL;
+	json_error_t err;
+	char *payload_str;
+	const char *signature_b64;
+	unsigned char *signature;
+	int rc, ret = -1, i = 0;
+	char tmp_file[] = ".aws.XXXXXX";
+	int tmp_fd;
+	size_t sig_size;
+
+	if (message_digest(info->checksum->calculate_sign(), region, region_count, &sha1, &sha1_len, 1)) {
+		fprintf(stderr, "%s: Failed to compute sha1 hash\n", __func__);
+		return -1;
+	}
+
+	payload = json_object();
+	if (!payload) {
+		fprintf(stderr, "%s: Failed to create json object\n", __func__);
+		goto out;
+	}
+
+	json_object_set_new(payload, "hash", json_string(sha1));
+	json_object_set_new(payload, "hash_type", json_string("SHA1"));
+	json_object_set_new(payload, "key_file", json_string(info->keyname));
+
+	payload_str = json_dumps(payload, 0);
+	if (!payload_str) {
+		fprintf(stderr, "%s: Failed to dump json object\n", __func__);
+		goto out;
+	}
+
+	tmp_fd = mkstemp(tmp_file);
+	if (tmp_fd < 0) {
+		fprintf(stderr, "%s: Failed to make temporary file: %s\n", __func__, strerror(errno));
+		goto out;
+	}
+	close(tmp_fd);
+
+	argv[i++] = "aws";
+	argv[i++] = "--profile";
+	argv[i++] = info->aws_profile ? info->aws_profile : DEFAULT_AWS_PROFILE;
+	argv[i++] = "lambda";
+	argv[i++] = "invoke";
+	argv[i++] = "--function-name";
+	argv[i++] = info->aws_function ? info->aws_function : DEFAULT_AWS_LAMBDA_FUNC;
+	argv[i++] = "--invocation-type";
+	argv[i++] = "RequestResponse";
+	argv[i++] = "--payload";
+	argv[i++] = payload_str;
+	argv[i++] = "--qualifier";
+	argv[i++] = "live";
+	argv[i++] = tmp_file;
+	argv[i++] = NULL;
+
+	rc = systemvp((char * const *) argv);
+	if (rc) {
+		fprintf(stderr, "%s: aws command failed rc=%d\n", __func__, rc);
+		goto out;
+	}
+
+	response = json_load_file(tmp_file, 0, &err);
+	if (!response) {
+		fprintf(stderr, "%s: failed to load %s: %s at line %d, column %d)\n",
+			__func__, tmp_file, err.text, err.line, err.column);
+		goto out;
+	}
+
+	json_dumpf(response, stdout, JSON_INDENT(2) | JSON_SORT_KEYS);
+	printf("\n");
+
+	signature_b64 = json_string_value(json_object_get(response, "signature"));
+	if (!signature_b64) {
+		goto out;
+	}
+
+	signature = base64_decode(signature_b64, strlen(signature_b64), &sig_size);
+	if (!signature) {
+		fprintf(stderr, "%s: failed to decode (base64) %s\n", __func__, signature_b64);
+		goto out;
+	}
+
+	*sig_len = sig_size;
+	*sigp = signature;
+
+	ret = 0;
+out:
+	if (sha1)
+		free(sha1);
+	if (payload)
+		json_decref(payload);
+	if (payload_str)
+		free(payload_str);
+	if (response)
+		json_decref(response);
+
+	if (unlink(tmp_file))
+		fprintf(stderr, "%s: Failed to unlink %s: %s\n", __func__, tmp_file, strerror(errno));
+
+	return ret;
+}
--- /dev/null
+++ b/lib/aws/utils.c
@@ -0,0 +1,207 @@
+#include <stdio.h>
+#include <stdlib.h>
+#include <sys/wait.h>
+#include <unistd.h>
+#include <image.h>
+#include <openssl/evp.h>
+
+#include "utils.h"
+
+static const unsigned char base64_table[65] =
+	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
+
+/**
+ * base64_decode - Base64 decode
+ * @src: Data to be decoded
+ * @len: Length of the data to be decoded
+ * @out_len: Pointer to output length variable
+ * Returns: Allocated buffer of out_len bytes of decoded data,
+ * or %NULL on failure
+ *
+ * Caller is responsible for freeing the returned buffer.
+ */
+unsigned char *base64_decode(const unsigned char *src, size_t len,
+			      size_t *out_len)
+{
+	unsigned char dtable[256], *out, *pos, block[4], tmp;
+	size_t i, count, olen;
+	int pad = 0;
+
+	memset(dtable, 0x80, 256);
+	for (i = 0; i < sizeof(base64_table) - 1; i++)
+		dtable[base64_table[i]] = (unsigned char) i;
+	dtable['='] = 0;
+
+	count = 0;
+	for (i = 0; i < len; i++) {
+		if (dtable[src[i]] != 0x80)
+			count++;
+	}
+
+	if (count == 0 || count % 4)
+		return NULL;
+
+	olen = count / 4 * 3;
+	pos = out = malloc(olen);
+	if (out == NULL)
+		return NULL;
+
+	count = 0;
+	for (i = 0; i < len; i++) {
+		tmp = dtable[src[i]];
+		if (tmp == 0x80)
+			continue;
+
+		if (src[i] == '=')
+			pad++;
+		block[count] = tmp;
+		count++;
+		if (count == 4) {
+			*pos++ = (block[0] << 2) | (block[1] >> 4);
+			*pos++ = (block[1] << 4) | (block[2] >> 2);
+			*pos++ = (block[2] << 6) | block[3];
+			count = 0;
+			if (pad) {
+				if (pad == 1)
+					pos--;
+				else if (pad == 2)
+					pos -= 2;
+				else {
+					/* Invalid padding */
+					free(out);
+					return NULL;
+				}
+				break;
+			}
+		}
+	}
+
+	*out_len = pos - out;
+	return out;
+}
+
+/*
+ *  Dumps buf into a string buffer as hex.
+ *  Caller should free returned string.
+ */
+char *hex_dump(char *buf, unsigned int buf_len) {
+	int i, hex_len, len = 0;
+	int ret;
+	char *hex;
+
+	hex_len = 2 * buf_len + 1;
+	hex = malloc(hex_len);
+	if (!hex) {
+		fprintf(stderr, "%s: not enough memory", __func__);
+		return NULL;
+	}
+
+	for (i = 0; i < buf_len; i++) {
+		ret = snprintf(hex+len, hex_len-len, "%02x", buf[i] & 0xff);
+		if (ret < 0 || ret >= hex_len-len) {
+			fprintf(stderr, "%s: hex dump truncated", __func__);
+			return hex;
+		}
+		len += ret;
+	}
+
+	return hex;
+}
+
+/*
+ * Digests msg using md
+ * On success, fills digest with the sha hash (which the caller must free)
+ * and returns 0. Returns -1 on failure.
+ *
+ * If hex is non-zero, digest is a hex string
+ *
+ * md should be one of the const EVP_* structs, eg. EVP_sha1()
+ * (see https://www.openssl.org/docs/man1.1.0/crypto/EVP_sha1.html)
+ */
+int message_digest(const EVP_MD *md, const struct image_region region[],
+		   int region_count, unsigned char **out_digest,
+		   unsigned int *out_digest_len, int hex)
+{
+	EVP_MD_CTX *ctx;
+	int i, ret = -1;
+	char *tmp;
+	unsigned char *digest;
+	unsigned int digest_len;
+
+	ctx = EVP_MD_CTX_create();
+	if (!ctx) {
+		fprintf(stderr, "Failed to create digest context\n");
+		return -1;
+	}
+
+	if (!EVP_DigestInit_ex(ctx, md, NULL)) {
+		fprintf(stderr, "Failed to init %s context\n", EVP_MD_name(md));
+		goto out;
+	}
+
+	digest = malloc(EVP_MD_size(md));
+	if (!digest) {
+		fprintf(stderr, "Failed to malloc digest\n");
+		goto out;
+	}
+
+	for (i = 0; i < region_count; i++) {
+		if (!EVP_DigestUpdate(ctx, region[i].data, region[i].size)) {
+			fprintf(stderr, "Failed to init %s context\n", EVP_MD_name(md));
+			free(digest);
+			goto out;
+		}
+	}
+
+	if (!EVP_DigestFinal_ex(ctx, digest, &digest_len)) {
+		fprintf(stderr, "Failed to finalize digest\n");
+		free(digest);
+		goto out;
+	}
+
+	if (hex) {
+		tmp = hex_dump(digest, digest_len);
+		free(digest);
+		if (!tmp)
+			goto out;
+		*out_digest = tmp;
+		*out_digest_len = strlen(tmp);
+	} else {
+		*out_digest = digest;
+		*out_digest_len = digest_len;
+	}
+
+	ret = 0;
+out:
+	EVP_MD_CTX_destroy(ctx);
+	return ret;
+}
+
+int systemvp(char * const *argv) {
+	pid_t pid;
+	int i, status;
+
+	for (i = 0; argv[i]; i++)
+		printf("%s ", argv[i]);
+	printf("\n");
+
+	pid = fork();
+	switch (pid) {
+	case -1:
+		fprintf(stderr, "fork failed\n");
+		return -1;
+	case 0: /* child */
+		execvp(argv[0], argv);
+		exit(1);
+	default: /* parent */
+		break;
+	}
+
+	wait(&status);
+	if (!WIFEXITED(status)) {
+		fprintf(stderr, "%s: %s exit abnormally, rc=%d\n", __func__, argv[0], WEXITSTATUS(status));
+		return -1;
+	}
+
+	return WEXITSTATUS(status);
+}
--- /dev/null
+++ b/lib/aws/utils.h
@@ -0,0 +1,13 @@
+#ifndef _UTILS_H
+#define _UTILS_H
+
+unsigned char *base64_decode(const unsigned char *src, size_t len,
+			     size_t *out_len);
+char *hex_dump(char *buf, unsigned int buf_len);
+int message_digest(const EVP_MD *md, const struct image_region region[],
+		   int region_count, unsigned char **digest,
+		   unsigned int *digest_len, int hex);
+int systemvp(char * const *argv);
+
+#endif /* _UTILS_H */
+
--- a/lib/rsa/rsa-sign.c
+++ b/lib/rsa/rsa-sign.c
@@ -12,14 +12,15 @@
 #include <openssl/bn.h>
 #include <openssl/rsa.h>
 #include <openssl/pem.h>
 #include <openssl/err.h>
 #include <openssl/ssl.h>
 #include <openssl/evp.h>
 #include <openssl/engine.h>
+#include <u-boot/aws.h>
 
 #if OPENSSL_VERSION_NUMBER >= 0x10000000L
 #define HAVE_ERR_REMOVE_THREAD_STATE
 #endif
 
 #if OPENSSL_VERSION_NUMBER < 0x10100000L
 static void RSA_get0_key(const RSA *r,
@@ -461,14 +462,18 @@ int rsa_sign(struct image_sign_info *inf
 	     const struct image_region region[], int region_count,
 	     uint8_t **sigp, uint *sig_len)
 {
 	RSA *rsa;
 	ENGINE *e = NULL;
 	int ret;
 
+	if (info->sign_type == SIGN_TYPE_AWS) {
+		return aws_sign(info, region, region_count, sigp, sig_len);
+	}
+
 	ret = rsa_init();
 	if (ret)
 		return ret;
 
 	if (info->engine_id) {
 		ret = rsa_engine_init(info->engine_id, &e);
 		if (ret)
--- a/tools/Makefile
+++ b/tools/Makefile
@@ -70,14 +70,15 @@ LIBFDT_SRCS_UNSYNCED := fdt_ro.c fdt_rw.
 
 LIBFDT_OBJS := $(addprefix libfdt/, $(patsubst %.c, %.o, $(LIBFDT_SRCS_SYNCED))) \
 	       $(addprefix lib/libfdt/, $(patsubst %.c, %.o, $(LIBFDT_SRCS_UNSYNCED)))
 
 RSA_OBJS-$(CONFIG_FIT_SIGNATURE) := $(addprefix lib/rsa/, \
 					rsa-sign.o rsa-verify.o rsa-checksum.o \
 					rsa-mod-exp.o)
+AWS_OBJS-$(CONFIG_FIT_SIGNATURE) := $(addprefix lib/aws/, aws-sign.o utils.o)
 
 ROCKCHIP_OBS = lib/rc4.o rkcommon.o rkimage.o rksd.o rkspi.o
 
 # common objs for dumpimage and mkimage
 dumpimage-mkimage-objs := aisimage.o \
 			atmelimage.o \
 			$(FIT_OBJS-y) \
@@ -106,15 +107,16 @@ dumpimage-mkimage-objs := aisimage.o \
 			common/hash.o \
 			ublimage.o \
 			zynqimage.o \
 			zynqmpimage.o \
 			$(LIBFDT_OBJS) \
 			gpimage.o \
 			gpimage-common.o \
-			$(RSA_OBJS-y)
+			$(RSA_OBJS-y) \
+			$(AWS_OBJS-y)
 
 dumpimage-objs := $(dumpimage-mkimage-objs) dumpimage.o
 mkimage-objs   := $(dumpimage-mkimage-objs) mkimage.o
 fit_info-objs   := $(dumpimage-mkimage-objs) fit_info.o
 fit_check_sign-objs   := $(dumpimage-mkimage-objs) fit_check_sign.o
 
 ifneq ($(CONFIG_MX23)$(CONFIG_MX28),)
@@ -137,14 +139,16 @@ ifneq ($(CONFIG_ARMADA_38X)$(CONFIG_ARMA
 HOSTCFLAGS_kwbimage.o += -DCONFIG_KWB_SECURE
 endif
 
 # MXSImage needs LibSSL
 ifneq ($(CONFIG_MX23)$(CONFIG_MX28)$(CONFIG_ARMADA_38X)$(CONFIG_ARMADA_39X)$(CONFIG_FIT_SIGNATURE),)
 HOSTLOADLIBES_mkimage += \
 	$(shell pkg-config --libs libssl libcrypto 2> /dev/null || echo "-lssl -lcrypto")
+HOSTLOADLIBES_mkimage += \
+	$(shell pkg-config --libs libjansson 2> /dev/null || echo "-ljansson")
 
 # OS X deprecate openssl in favour of CommonCrypto, supress deprecation
 # warnings on those systems
 ifeq ($(HOSTOS),darwin)
 HOSTCFLAGS_mxsimage.o += -Wno-deprecated-declarations
 HOSTCFLAGS_image-sig.o += -Wno-deprecated-declarations
 HOSTCFLAGS_rsa-sign.o += -Wno-deprecated-declarations
--- a/tools/fit_image.c
+++ b/tools/fit_image.c
@@ -53,18 +53,15 @@ static int fit_add_file_data(struct imag
 	/* for first image creation, add a timestamp at offset 0 i.e., root  */
 	if (params->datafile) {
 		time_t time = imagetool_get_source_date(params, sbuf.st_mtime);
 		ret = fit_set_timestamp(ptr, 0, time);
 	}
 
 	if (!ret) {
-		ret = fit_add_verification_data(params->keydir, dest_blob, ptr,
-						params->comment,
-						params->require_keys,
-						params->engine_id);
+		ret = fit_add_verification_data(params, dest_blob, ptr);
 	}
 
 	if (dest_blob) {
 		munmap(dest_blob, destfd_size);
 		close(destfd);
 	}
 
--- a/tools/image-host.c
+++ b/tools/image-host.c
@@ -144,37 +144,40 @@ static int fit_image_write_sig(void *fit
 		}
 	}
 
 	return ret;
 }
 
 static int fit_image_setup_sig(struct image_sign_info *info,
-		const char *keydir, void *fit, const char *image_name,
-		int noffset, const char *require_keys, const char *engine_id)
+		struct image_tool_params *params, void *fit, const char *image_name,
+		int noffset, const char *require_keys)
 {
 	const char *node_name;
 	char *algo_name;
 
 	node_name = fit_get_name(fit, noffset, NULL);
 	if (fit_image_hash_get_algo(fit, noffset, &algo_name)) {
 		printf("Can't get algo property for '%s' signature node in '%s' image node\n",
 		       node_name, image_name);
 		return -1;
 	}
 
 	memset(info, '\0', sizeof(*info));
-	info->keydir = keydir;
+	info->keydir = params->keydir;
 	info->keyname = fdt_getprop(fit, noffset, "key-name-hint", NULL);
 	info->fit = fit;
 	info->node_offset = noffset;
 	info->name = strdup(algo_name);
 	info->checksum = image_get_checksum_algo(algo_name);
 	info->crypto = image_get_crypto_algo(algo_name);
-	info->require_keys = require_keys;
-	info->engine_id = engine_id;
+	info->require_keys = params->require_keys ? require_keys : NULL;
+	info->engine_id = params->engine_id;
+	info->sign_type = params->sign_type;
+	info->aws_profile = params->aws_profile;
+	info->aws_function = params->aws_function;
 	if (!info->checksum || !info->crypto) {
 		printf("Unsupported signature algorithm (%s) for '%s' signature node in '%s' image node\n",
 		       algo_name, node_name, image_name);
 		return -1;
 	}
 
 	return 0;
@@ -194,28 +197,26 @@ static int fit_image_setup_sig(struct im
  * @data:	data to process
  * @size:	size of data in bytes
  * @comment:	Comment to add to signature nodes
  * @require_keys: Mark all keys as 'required'
  * @engine_id:	Engine to use for signing
  * @return 0 if ok, -1 on error
  */
-static int fit_image_process_sig(const char *keydir, void *keydest,
+static int fit_image_process_sig(struct image_tool_params *params, void *keydest,
 		void *fit, const char *image_name,
-		int noffset, const void *data, size_t size,
-		const char *comment, int require_keys, const char *engine_id)
+		int noffset, const void *data, size_t size)
 {
 	struct image_sign_info info;
 	struct image_region region;
 	const char *node_name;
 	uint8_t *value;
 	uint value_len;
 	int ret;
 
-	if (fit_image_setup_sig(&info, keydir, fit, image_name, noffset,
-				require_keys ? "image" : NULL, engine_id))
+	if (fit_image_setup_sig(&info, params, fit, image_name, noffset, "image"))
 		return -1;
 
 	node_name = fit_get_name(fit, noffset, NULL);
 	region.data = data;
 	region.size = size;
 	ret = info.crypto->sign(&info, &region, 1, &value, &value_len);
 	if (ret) {
@@ -224,15 +225,15 @@ static int fit_image_process_sig(const c
 
 		/* We allow keys to be missing */
 		if (ret == -ENOENT)
 			return 0;
 		return -1;
 	}
 
-	ret = fit_image_write_sig(fit, noffset, value, value_len, comment,
+	ret = fit_image_write_sig(fit, noffset, value, value_len, params->comment,
 			NULL, 0);
 	if (ret) {
 		if (ret == -FDT_ERR_NOSPACE)
 			return -ENOSPC;
 		printf("Can't write signature for '%s' signature node in '%s' conf node: %s\n",
 		       node_name, image_name, fdt_strerror(ret));
 		return -1;
@@ -290,17 +291,16 @@ static int fit_image_process_sig(const c
  * @fit:	Pointer to the FIT format image header
  * @image_noffset: Requested component image node
  * @comment:	Comment to add to signature nodes
  * @require_keys: Mark all keys as 'required'
  * @engine_id:	Engine to use for signing
  * @return: 0 on success, <0 on failure
  */
-int fit_image_add_verification_data(const char *keydir, void *keydest,
-		void *fit, int image_noffset, const char *comment,
-		int require_keys, const char *engine_id)
+int fit_image_add_verification_data(struct image_tool_params *params,
+		void *keydest, void *fit, int image_noffset)
 {
 	const char *image_name;
 	const void *data;
 	size_t size;
 	int noffset;
 
 	/* Get image data and data length */
@@ -324,20 +324,19 @@ int fit_image_add_verification_data(cons
 		 * names, e.g. hash@1, hash@2, signature@1, etc.
 		 */
 		node_name = fit_get_name(fit, noffset, NULL);
 		if (!strncmp(node_name, FIT_HASH_NODENAME,
 			     strlen(FIT_HASH_NODENAME))) {
 			ret = fit_image_process_hash(fit, image_name, noffset,
 						data, size);
-		} else if (IMAGE_ENABLE_SIGN && keydir &&
+		} else if (IMAGE_ENABLE_SIGN && params->keydir &&
 			   !strncmp(node_name, FIT_SIG_NODENAME,
 				strlen(FIT_SIG_NODENAME))) {
-			ret = fit_image_process_sig(keydir, keydest,
-				fit, image_name, noffset, data, size,
-				comment, require_keys, engine_id);
+			ret = fit_image_process_sig(params, keydest,
+				fit, image_name, noffset, data, size);
 		}
 		if (ret)
 			return ret;
 	}
 
 	return 0;
 }
@@ -567,18 +566,16 @@ static int fit_config_get_data(void *fit
 	*regionp = region;
 	*region_propp = region_prop;
 	*region_proplen = len;
 
 	return 0;
 }
 
-static int fit_config_process_sig(const char *keydir, void *keydest,
-		void *fit, const char *conf_name, int conf_noffset,
-		int noffset, const char *comment, int require_keys,
-		const char *engine_id)
+static int fit_config_process_sig(struct image_tool_params *params, void *keydest,
+		void *fit, const char *conf_name, int conf_noffset, int noffset)
 {
 	struct image_sign_info info;
 	const char *node_name;
 	struct image_region *region;
 	char *region_prop;
 	int region_proplen;
 	int region_count;
@@ -587,16 +584,15 @@ static int fit_config_process_sig(const
 	int ret;
 
 	node_name = fit_get_name(fit, noffset, NULL);
 	if (fit_config_get_data(fit, conf_noffset, noffset, &region,
 				&region_count, &region_prop, &region_proplen))
 		return -1;
 
-	if (fit_image_setup_sig(&info, keydir, fit, conf_name, noffset,
-				require_keys ? "conf" : NULL, engine_id))
+	if (fit_image_setup_sig(&info, params, fit, conf_name, noffset, "conf"))
 		return -1;
 
 	ret = info.crypto->sign(&info, region, region_count, &value,
 				&value_len);
 	free(region);
 	if (ret) {
 		printf("Failed to sign '%s' signature node in '%s' conf node\n",
@@ -604,15 +600,15 @@ static int fit_config_process_sig(const
 
 		/* We allow keys to be missing */
 		if (ret == -ENOENT)
 			return 0;
 		return -1;
 	}
 
-	ret = fit_image_write_sig(fit, noffset, value, value_len, comment,
+	ret = fit_image_write_sig(fit, noffset, value, value_len, params->comment,
 				region_prop, region_proplen);
 	if (ret) {
 		if (ret == -FDT_ERR_NOSPACE)
 			return -ENOSPC;
 		printf("Can't write signature for '%s' signature node in '%s' conf node: %s\n",
 		       node_name, conf_name, fdt_strerror(ret));
 		return -1;
@@ -632,17 +628,16 @@ static int fit_config_process_sig(const
 		}
 		return ret;
 	}
 
 	return 0;
 }
 
-static int fit_config_add_verification_data(const char *keydir, void *keydest,
-		void *fit, int conf_noffset, const char *comment,
-		int require_keys, const char *engine_id)
+static int fit_config_add_verification_data(struct image_tool_params *params, void *keydest,
+		void *fit, int conf_noffset)
 {
 	const char *conf_name;
 	int noffset;
 
 	conf_name = fit_get_name(fit, conf_noffset, NULL);
 
 	/* Process all hash subnodes of the configuration node */
@@ -651,28 +646,26 @@ static int fit_config_add_verification_d
 	     noffset = fdt_next_subnode(fit, noffset)) {
 		const char *node_name;
 		int ret = 0;
 
 		node_name = fit_get_name(fit, noffset, NULL);
 		if (!strncmp(node_name, FIT_SIG_NODENAME,
 			     strlen(FIT_SIG_NODENAME))) {
-			ret = fit_config_process_sig(keydir, keydest,
-				fit, conf_name, conf_noffset, noffset, comment,
-				require_keys, engine_id);
+			ret = fit_config_process_sig(params, keydest,
+				fit, conf_name, conf_noffset, noffset);
 		}
 		if (ret)
 			return ret;
 	}
 
 	return 0;
 }
 
-int fit_add_verification_data(const char *keydir, void *keydest, void *fit,
-			      const char *comment, int require_keys,
-			      const char *engine_id)
+int fit_add_verification_data(struct image_tool_params *params, void *keydest,
+			      void *fit)
 {
 	int images_noffset, confs_noffset;
 	int noffset;
 	int ret;
 
 	/* Find images parent node offset */
 	images_noffset = fdt_path_offset(fit, FIT_IMAGES_PATH);
@@ -686,40 +679,36 @@ int fit_add_verification_data(const char
 	for (noffset = fdt_first_subnode(fit, images_noffset);
 	     noffset >= 0;
 	     noffset = fdt_next_subnode(fit, noffset)) {
 		/*
 		 * Direct child node of the images parent node,
 		 * i.e. component image node.
 		 */
-		ret = fit_image_add_verification_data(keydir, keydest,
-				fit, noffset, comment, require_keys, engine_id);
+		ret = fit_image_add_verification_data(params, keydest, fit, noffset);
 		if (ret)
 			return ret;
 	}
 
 	/* If there are no keys, we can't sign configurations */
-	if (!IMAGE_ENABLE_SIGN || !keydir)
+	if (!IMAGE_ENABLE_SIGN || !params->keydir)
 		return 0;
 
 	/* Find configurations parent node offset */
 	confs_noffset = fdt_path_offset(fit, FIT_CONFS_PATH);
 	if (confs_noffset < 0) {
 		printf("Can't find images parent node '%s' (%s)\n",
 		       FIT_CONFS_PATH, fdt_strerror(confs_noffset));
 		return -ENOENT;
 	}
 
 	/* Process its subnodes, print out component images details */
 	for (noffset = fdt_first_subnode(fit, confs_noffset);
 	     noffset >= 0;
 	     noffset = fdt_next_subnode(fit, noffset)) {
-		ret = fit_config_add_verification_data(keydir, keydest,
-						       fit, noffset, comment,
-						       require_keys,
-						       engine_id);
+		ret = fit_config_add_verification_data(params, keydest, fit, noffset);
 		if (ret)
 			return ret;
 	}
 
 	return 0;
 }
 
--- a/tools/imagetool.h
+++ b/tools/imagetool.h
@@ -60,14 +60,17 @@ struct image_tool_params {
 	char *imagename2;
 	char *datafile;
 	char *imagefile;
 	char *cmdname;
 	const char *outfile;	/* Output filename */
 	const char *keydir;	/* Directory holding private keys */
 	const char *keydest;	/* Destination .dtb for public key */
+	int sign_type;		/* Signing type */
+	const char *aws_profile;	/* AWS profile to use */
+	const char *aws_function;	/* AWS lambda function to use */
 	const char *comment;	/* Comment to add to signature node */
 	int require_keys;	/* 1 to mark signing keys as 'required' */
 	int file_size;		/* Total size of output file */
 	int orig_file_size;	/* Original size for file before padding */
 	bool auto_its;		/* Automatically create the .its file */
 	int fit_image_type;	/* Image type to put into the FIT */
 	char *fit_ramdisk;	/* Ramdisk file to include */
--- /dev/null
+++ b/tools/lib/aws/aws-sign.c
@@ -0,0 +1 @@
+#include <../lib/aws/aws-sign.c>
--- a/tools/mkimage.c
+++ b/tools/mkimage.c
@@ -103,14 +103,19 @@ static void usage(const char *msg)
 		"          -k => set directory containing private keys\n"
 		"          -K => write public keys to this .dtb file\n"
 		"          -c => add comment in signature node\n"
 		"          -F => re-sign existing FIT image\n"
 		"          -p => place external data at a static position\n"
 		"          -r => mark keys used as 'required' in dtb\n"
 		"          -N => engine to use for signing (pkcs11)\n");
+	fprintf(stderr,
+		"Extended Signing options: [-t rsa_file|-t aws] [-P aws_profile] [-L aws_lambda_function]\n"
+		"          -t => Signing type\n"
+		"          -P => AWS profile to use\n"
+		"          -L => AWS lambda function to use\n");
 #else
 	fprintf(stderr,
 		"Signing / verified boot not supported (CONFIG_FIT_SIGNATURE undefined)\n");
 #endif
 	fprintf(stderr, "       %s -V ==> print version information and exit\n",
 		params.cmdname);
 	fprintf(stderr, "Use '-T list' to see a list of available image types\n");
@@ -140,15 +145,15 @@ static void process_args(int argc, char
 {
 	char *ptr;
 	int type = IH_TYPE_INVALID;
 	char *datafile = NULL;
 	int opt;
 
 	while ((opt = getopt(argc, argv,
-			     "a:A:b:c:C:d:D:e:Ef:Fk:i:K:ln:N:p:O:rR:qsT:vVx")) != -1) {
+			     "a:A:b:c:C:d:D:e:Ef:Fk:i:K:lL:n:N:p:P:O:rR:qsT:t:vVx")) != -1) {
 		switch (opt) {
 		case 'a':
 			params.addr = strtoull(optarg, &ptr, 16);
 			if (*ptr) {
 				fprintf(stderr, "%s: invalid load address %s\n",
 					params.cmdname, optarg);
 				exit(EXIT_FAILURE);
@@ -218,14 +223,17 @@ static void process_args(int argc, char
 			break;
 		case 'K':
 			params.keydest = optarg;
 			break;
 		case 'l':
 			params.lflag = 1;
 			break;
+		case 'L':
+			params.aws_function = optarg;
+			break;
 		case 'n':
 			params.imagename = optarg;
 			break;
 		case 'N':
 			params.engine_id = optarg;
 			break;
 		case 'O':
@@ -239,14 +247,17 @@ static void process_args(int argc, char
 			params.external_offset = strtoull(optarg, &ptr, 16);
 			if (*ptr) {
 				fprintf(stderr, "%s: invalid offset size %s\n",
 					params.cmdname, optarg);
 				exit(EXIT_FAILURE);
 			}
 			break;
+		case 'P':
+			params.aws_profile = optarg;
+			break;
 		case 'q':
 			params.quiet = 1;
 			break;
 		case 'r':
 			params.require_keys = 1;
 			break;
 		case 'R':
@@ -266,14 +277,22 @@ static void process_args(int argc, char
 			}
 			type = genimg_get_type_id(optarg);
 			if (type < 0) {
 				show_valid_options(IH_TYPE);
 				usage("Invalid image type");
 			}
 			break;
+		case 't':
+			if (strcmp(optarg, "aws") == 0)
+				params.sign_type = SIGN_TYPE_AWS;
+			else if (strcmp(optarg, "rsa_file") == 0)
+				params.sign_type = SIGN_TYPE_RSA_FILE;
+			else
+				usage("Invalid signing type");
+			break;
 		case 'v':
 			params.vflag++;
 			break;
 		case 'V':
 			printf("mkimage version %s\n", PLAIN_VERSION);
 			exit(EXIT_SUCCESS);
 		case 'x':
@@ -317,14 +336,15 @@ int main(int argc, char **argv)
 	struct image_type_params *tparams = NULL;
 	int pad_len = 0;
 	int dfd;
 
 	params.cmdname = *argv;
 	params.addr = 0;
 	params.ep = 0;
+	params.sign_type = SIGN_TYPE_RSA_FILE;
 
 	process_args(argc, argv);
 
 	/* set tparams as per input type_id */
 	tparams = imagetool_get_type(params.type);
 	if (tparams == NULL) {
 		fprintf (stderr, "%s: unsupported type %s\n",
